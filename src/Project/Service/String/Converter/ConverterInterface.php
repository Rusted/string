<?php declare(strict_types = 1);

namespace App\Service\String\Converter;

interface ConverterInterface
{
    /**
     * @param string $input
     * @return string
     */
    public function convert(string $input): string;
}
