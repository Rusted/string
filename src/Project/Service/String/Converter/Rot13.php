<?php declare(strict_types = 1);

namespace App\Service\String\Converter;

class Rot13 implements ConverterInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function convert(string $string): string
    {
        return str_rot13($string);
    }
}
