<?php declare(strict_types = 1);

namespace App\Service\String\Converter;

class LettersToNumbers implements ConverterInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function convert(string $string): string
    {
        $convertedString = '';
        for ($i = 0; $i <= strlen($string) - 1; $i++) {
            $char = substr($string, $i, 1);
            $asciiNumber = ord($char);
            if (($asciiNumber >= 97 && $asciiNumber <= 122) || ($asciiNumber >= 65 && $asciiNumber <= 90)) {
                $convertedString .= "/" . ($asciiNumber - 96);
            } else {
                $convertedString .= $char;
            }
        }

        return $convertedString;
    }
}
