<?php declare(strict_types = 1);

namespace App\Service\String\Generator;

class LetterNumber
{
    const ABC = 'abcdefghijklmnopqrstuvwxyz0123456789';

    const MAX_LENGTH = 10000;

    const MAX_ARRAY_SIZE = 1000;

    protected $length;

    protected $arraySize;

    protected $converter;

    /**
     * LetterNumber constructor.
     * @param int $length
     * @param int $arraySize
     * @throws \Exception
     */
    public function __construct(int $length = 10, int $arraySize = 10)
    {
        if ($length < 1) {
            throw new \Exception('length must be positive');
        }

        if ($arraySize < 1) {
            throw new \Exception('arraySize must be positive');
        }

        if ($arraySize > self::MAX_ARRAY_SIZE ) {
            throw new \Exception('arraySize must be smaller than ' . self::MAX_ARRAY_SIZE);
        }

        if ($length > self::MAX_LENGTH ) {
            throw new \Exception('length must be smaller than ' . self::MAX_LENGTH);
        }

        $this->length = $length;
        $this->arraySize = $arraySize;
    }

    /**
     * @return string
     */
    public function generate(): string
    {
        $result = '';
        for ($i = 1; $i <= $this->length; $i++) {
            $randomCharPosition = mt_rand(0, strlen(self::ABC) - 1);
            $result .= substr(self::ABC, $randomCharPosition, 1);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function generateArray(): array
    {
        $result = [];
        for ($i = 1; $i <= $this->arraySize; $i++) {
            $result[] = $this->generate();
        }

        return $result;
    }
}
