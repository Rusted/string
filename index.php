<?php declare(strict_types = 1);
require_once 'vendor/autoload.php';
//Demo
use Symfony\Component\DependencyInjection\ContainerBuilder;

$containerBuilder = new ContainerBuilder();
$containerBuilder
    ->register('generator', \App\Service\String\Generator\LetterNumber::class)
    ->addArgument('%generator.length%')
    ->addArgument('%generator.arraySize%')
;

$containerBuilder
    ->register('converter.rot13', \App\Service\String\Converter\Rot13::class)
;

$containerBuilder
    ->register('converter.lettersToNumbers', \App\Service\String\Converter\LettersToNumbers::class)
;

$rot13Converter = $containerBuilder->get('converter.rot13');
$lettersToNumbersConverter = $containerBuilder->get('converter.lettersToNumbers');


for($i = 1; $i <= 10; $i++) {
    $length = mt_rand(1,100);
    $arraySize = mt_rand(1, 10);
    $containerBuilder
        ->register('generator_' . $i, \App\Service\String\Generator\LetterNumber::class)
        ->addArgument($length)
        ->addArgument($arraySize)
    ;

    if (mt_rand(0, 1)) {
        $converterName = 'letters to numbers';
        $converter = $lettersToNumbersConverter;
    } else {
        $converter = $rot13Converter;
        $converterName = 'rot13';
    }

    /**
     * @var App\Service\String\Generator\LetterNumber $generator
     * @var App\Service\String\Converter\LettersToNumbers|App\Service\String\Converter\Rot13 $converter
     */
    $generator = $containerBuilder->get('generator_' . $i);
    $generatedString = $generator->generate();
    $convertedString = $converter->convert($generatedString);

    echo "Generator $i generated $generatedString, converted to $convertedString using $converterName converter" . PHP_EOL;

}

