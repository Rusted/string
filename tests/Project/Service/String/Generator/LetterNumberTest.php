<?php declare(strict_types = 1);

namespace App\Tests\Service\String\Generator;

use PHPUnit\Framework\TestCase;
use App\Service\String\Generator\LetterNumber as Generator;

class LettersToNumbersTest extends TestCase
{
    /**
     * @return array
     */
    public function constructorDataProvider(): array
    {
        return [
            [10, 10, false],
            [0, 10, true],
            [10, 0, true],
            [10, -1, true],
            [-1, 10, true],
            [10001, 10, true],
            [10, 10000, true],
            [10, 1000, false],
            [10000, 1000, false],
        ];
    }

    /**
     * @dataProvider constructorDataProvider
     * @param int $length
     * @param int $arraySize
     * @param bool $exceptionClass
     * @throws \Exception
     */
    public function testConstructor(int $length, int $arraySize, bool $expectsException): void
    {
        if ($expectsException) {
            $this->expectException(\Exception::class);
        } else {
            $this->assertTrue(true); // to hide "no tests were executed" warning
        }

        $generator = new Generator($length, $arraySize);
    }


    /**
     * @return array
     */
    public function generateDataProvider(): array
    {
        return [
            [1, 1],
            [2, 1],
            [5, 1],
            [10, 1],
            [100, 1],
            [1000, 1],
            [10000, 1]
        ];
    }

    /**
     * @dataProvider generateDataProvider
     * @param int $length
     * @param int $arraySize
     * @throws \Exception
     */
    public function testGenerate(int $length, int $arraySize): void
    {
        $generator = new Generator($length, $arraySize);
        $generatedString = $generator->generate();
        $this->checkString($generatedString, $length);
    }

    /**
     * @return array
     */
    public function generateArrayDataProvider(): array
    {
        return [
            [1, 10],
            [2, 100],
            [5, 1000],
            [10, 1000],
            [100, 1000],
            [1000, 1000],
            [10000, 1000]
        ];
    }

    /**
     * @dataProvider generateArrayDataProvider
     * @param int $length
     * @param int $arraySize
     * @throws \Exception
     */
    public function testGenerateArray(int $length, int $arraySize): void
    {
        $generator = new Generator($length, $arraySize);
        $generatedArray = $generator->generateArray();
        $this->assertEquals($arraySize, count($generatedArray), 'Generated array should have length equal to generators arraySize');
        foreach ($generatedArray as $generatedString) {
            $this->checkString($generatedString, $length);
        }
    }

    /**
     * @param string $string
     * @param int $length
     */
    protected function checkString(string $string, int $length): void
    {
        $this->assertRegExp('/^[a-z0-9]+$/', $string,'Generated string must only contain lowercase characters and numbers');
        $this->assertEquals($length, strlen($string), 'Generated string should have length equal to generators');
    }
}
