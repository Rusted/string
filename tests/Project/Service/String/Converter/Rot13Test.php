<?php declare(strict_types = 1);

namespace App\Tests\Service\String\Converter;

use PHPUnit\Framework\TestCase;
use App\Service\String\Converter\Rot13;

class Rot13Test extends TestCase
{
    /**
     * @return array
     */
    public function convertDataProvider(): array
    {
        return [
            ['', ''],
            ['abcdefghijklmnopqrstuvwxyz1234567890', 'nopqrstuvwxyzabcdefghijklm1234567890'],
        ];
    }

    /**
     * @dataProvider convertDataProvider
     * @param string $input
     * @param string $output
     */
    public function testConvert(string $input, string $output): void
    {
        $converter = new Rot13();
        $this->assertEquals($converter->convert($input), $output);
    }
}
