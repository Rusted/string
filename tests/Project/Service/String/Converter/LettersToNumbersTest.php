<?php declare(strict_types = 1);

namespace App\Tests\Service\String\Converter;

use PHPUnit\Framework\TestCase;
use App\Service\String\Converter\LettersToNumbers;

class LettersToNumbersTest extends TestCase
{
    /**
     * @return array
     */
    public function convertDataProvider(): array
    {
        return [
            ['', ''],
            ['abcdefghijklmnopqrstuvwxyz1234567890', '/1/2/3/4/5/6/7/8/9/10/11/12/13/14/15/16/17/18/19/20/21/22/23/24/25/261234567890'],
            ['123', '123'],
            ['aae', '/1/1/5']
        ];
    }

    /**
     * @dataProvider convertDataProvider
     * @param string $input
     * @param string $output
     */
    public function testConvert(string $input, string $output): void
    {
        $converter = new LettersToNumbers();
        $this->assertEquals($converter->convert($input), $output);
    }
}
